package model;

import java.util.UUID;

public class Company extends BaseModel {
    private String companyName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String toString() {
        return "Company{" +
                "companyName='" + companyName + '\'' +
                ", Id=" + super.Id +
                '}';
    }
}
