package model;

import java.util.UUID;

public class Employee extends BaseModel{
    private String name;
    private UUID managerId;
    private UUID companyId;

    public UUID getCompanyId() {
        return companyId;
    }

    public void setCompanyId(UUID companyId) {
        this.companyId = companyId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setManagerId(UUID managerId) {
        this.managerId = managerId;
    }

    public String getName() {
        return name;
    }

    public UUID getManagerId() {
        return managerId;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                "id='" + super.Id + '\'' +
                ", managerId=" + managerId +
                ", companyId=" + companyId +
                '}';
    }
}
