package model;

import java.util.UUID;

public abstract class BaseModel {
     protected final UUID Id;

     public BaseModel() {
          this.Id = UUID.randomUUID();
     }

     public UUID getId() {
          return Id;
     }



}
