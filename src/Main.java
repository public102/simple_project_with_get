import model.Company;
import model.Employee;
import service.CompanyService;
import service.EmployeeService;

import java.util.Scanner;
import java.util.UUID;

public class Main {
    static  Scanner scannerInt = new Scanner(System.in);
    static Scanner scannerStr = new Scanner(System.in);
    static Scanner scannerId = new Scanner(System.in);
    public static void main(String[] args) {
        CompanyService companyService = new CompanyService();
        EmployeeService employeeService = new EmployeeService();
        int step = 10;
        while(step!=0){
            System.out.println("1.Create company  2.Update Company  3.Delete Company  0.Exit");
            step = scannerInt.nextInt();
            if(step==1){
                Company company = addCompany();
                companyService.addCompany(company);
                System.out.println(company);
                int inStep = 10;
                while(inStep!=0){
                    System.out.println("1.Add employee  2.Set manager  3.Add manager to employee  4.Get employees by managerId  5.Delete manager  0.Exit");
                    inStep = scannerInt.nextInt();
                    switch (inStep){
                        case 1 ->{
                            Employee employee = addEmployee(company.getId());
                            employeeService.addEmployee(employee);
                            System.out.println(employee);
                        }
                        case 2 ->{
                            setManager(employeeService.getEmployees());
                        }
                        case 3 ->{
                            System.out.println("Enter employee name you wanna set manager to him");
                            String name = scannerStr.nextLine();
                            System.out.println("Enter manager id to be manager of this employee");
                            String managerId = scannerId.next();
                            employeeService.setManager(UUID.fromString(managerId),name);
                            Employee employee =employeeService.findEmployeeByName(name);
                            System.out.println(employee);
                        }
                        case 4 ->{
                            System.out.println("Enter manager id");
                            String managerId = scannerId.next();
                            Employee[] employees = employeeService.getEmployeesByManagerId(UUID.fromString(managerId));
                            getEmployeesByManagerId(employees);
                        }
                        case 5 -> {
                            System.out.println("Enter manager Id");
                            String id = scannerStr.next();
                            /*System.out.println("Enter manager name");
                            String managerName = scannerStr.nextLine();*/
                            employeeService.deleteManager(UUID.fromString(id));
                        }
                    }
                }

            } else if (step==2) {
                System.out.println("Enter company name");
                String companyName = scannerStr.nextLine();
                System.out.println("Enter new name");
                String newName = scannerStr.nextLine();
                if(companyService.updateCompany(companyName,newName)){
                    System.out.println("Company successfully updated!!!");
                }else{
                    System.out.println("Something wrong please try again!");
                }
            } else if (step==3) {
                System.out.println("Enter company name");
                String name = scannerStr.nextLine();
                companyService.deleteCompany(name);
            }
        }
    }
    public static Company addCompany(){
        Company company = new Company();
        System.out.println("Enter name ");
        company.setCompanyName(scannerStr.nextLine());
        return company;

    }
    public static Employee addEmployee(UUID companyId){
        Employee employee = new Employee();
        System.out.println("Enter employee name");
        employee.setName(scannerStr.nextLine());
        employee.setCompanyId(companyId);
        return employee;
    }
    public static void setManager(Employee[] employees){
        System.out.println("Enter employee name that you wanna set manager position ");
        String name = scannerStr.nextLine();
        for (int i = 0; i < employees.length; i++) {
            if(employees[i]!=null&&employees[i].getName().equals(name)){
                System.out.println("This employee id is(you should use this id as manager id to other employees) "+employees[i].getId());
            }
        }
    }
    public static void getEmployeesByManagerId(Employee[] employees){
        for (int i = 0; i < employees.length; i++) {
            if(employees[i]!=null){
                System.out.println(i+". "+employees[i]);
            }
        }
    }
}