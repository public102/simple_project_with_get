package service;

import model.Employee;

import java.util.UUID;

public class EmployeeService {
    public Employee[] employees = new Employee[100];
    private int indexEmployee = 0;

    public boolean addEmployee(Employee employee){
        if (!hasEmployee(employee.getName())){
            employees[indexEmployee++]=employee;
            return true;
        }
        return false;
    }
    private boolean hasEmployee(String name){
        for(Employee employee : employees){
            if(employee!=null&&employee.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    public Employee[] getEmployeesByManagerId(UUID managerId){
        int indexEmployees1 =0;
        for (int i = 0; i < employees.length; i++) {
            if(employees[i]!=null&&employees[i].getManagerId()!=null){
                if(employees[i].getManagerId().equals(managerId)){
                    indexEmployees1++;
                }
            }
        }
        Employee[] employees1 = new Employee[indexEmployees1];
        int inIndex = 0;
        for (int i = 0; i < indexEmployee; i++) {
            if(employees[i]!=null&&employees[i].getManagerId()!=null){
                if(employees[i].getManagerId().equals(managerId)){
                    employees1[inIndex++]=employees[i];
                }
            }
        }
        return employees1;
    }

    public void setManager(UUID id,String name){
        for (int i = 0; i < indexEmployee; i++) {
            if (employees[i]!=null&&employees[i].getName().equals(name)){
                employees[i].setManagerId(id);
            }
        }
    }
    public void deleteManager(UUID managerId){
        /*for (int i = 0; i < indexEmployee; i++) {
            if(employees[i]!=null&&employees[i].getManagerId()!=null){
                if(employees[i].getName().equals(managerName)){
                    employees[i]=null;
                }
            }
        }*/
        for (int i = 0; i < indexEmployee; i++) {
            if(employees[i]!=null&&employees[i].getManagerId()!=null){
                if(employees[i].getManagerId().equals(managerId)){
                    employees[i]=null;
                }
            }
        }
    }
    public Employee[] getEmployees(){
        return employees;
    }
    public Employee findEmployeeByName(String name){
        for (int i = 0; i < indexEmployee; i++) {
            if(employees[i]!=null&&employees[i].getName().equals(name)){
                return employees[i];
            }
        }
        return null;
    }

}
