package service;

import model.Company;

public class CompanyService {
    public Company[] companies = new Company[100];
    private int indexCompany = 0;
     public boolean addCompany(Company company){
         if(!hasCompany(company.getCompanyName())){
             companies[indexCompany++]=company;
             return true;
         }
         return false;
     }
     private boolean hasCompany(String name){
         for(Company company : companies){
             if(company!=null&&company.getCompanyName().equals(name)){
                 return true;
             }
         }
         return false;
     }
     public void deleteCompany(String companyName){
         int index = 0;
         for (int i = 0; i < indexCompany; i++) {
             if(companies[i]!=null&&companies[i].getCompanyName().equals(companyName)){
                 index=i;
                 companies[i]=null;
             }
         }
         for (int i = index; i < indexCompany-1; i++) {
             companies[i]=companies[i+1];
         }
     }
     public boolean updateCompany(String name,String newName){
         for(Company company : companies){
             if(company!=null&&company.getCompanyName().equals(name)){
                 company.setCompanyName(newName);
                 return true;
             }
         }
         return false;
     }

}
